package dsfn.edu.testapp;


import android.app.Activity;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.vipul.hp_hp.timelineview.TimelineView;

import java.util.Date;
import java.util.Random;

import dsfn.edu.testapp.Utils.DateUtils;
import dsfn.edu.testapp.models.RequestDetails;
import dsfn.edu.testapp.tasks.RequestDetailsLoader;


public class RequestDetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<RequestDetails> {
    public static final String ARG_ITEM_ID = "item_id";

    private CollapsingToolbarLayout appBarLayout;

    private TextView mStatus;
    private TextView mFullName;
    private TextView mDescription;
    private EditText mSolutionDescription;

    private ProgressBar mTimeLine;

    private TextView mCreatedAt;
    private TextView mActualRecovery;
    private TextView mSLARecovery;

    public RequestDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {

            String requestItemID = getArguments().getString(ARG_ITEM_ID);

            Bundle bndl = new Bundle();
            bndl.putString(RequestDetailsLoader.REQ_ITEM_ID, requestItemID);
            getLoaderManager().initLoader(RequestDetailsLoader.RDLOADER_ID, bndl, this).startLoading();

            Activity activity = this.getActivity();
            appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle("Details");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.request_detail, container, false);

        mStatus = (TextView) rootView.findViewById(R.id.req_status);
        mFullName = (TextView) rootView.findViewById(R.id.full_name);
        mDescription = (TextView) rootView.findViewById(R.id.req_description);
        mSolutionDescription = (EditText) rootView.findViewById(R.id.req_solution_description);
        mTimeLine = (ProgressBar) rootView.findViewById(R.id.time_expired);
        mCreatedAt = (TextView) rootView.findViewById(R.id.created_at);
        mActualRecovery = (TextView) rootView.findViewById(R.id.actual);
        mSLARecovery = (TextView) rootView.findViewById(R.id.sla_rec);

        return rootView;
    }

    @Override
    public android.support.v4.content.Loader<RequestDetails> onCreateLoader(int id, Bundle args) {
        if(id == RequestDetailsLoader.RDLOADER_ID){
            return new RequestDetailsLoader(this.getActivity(), args);
        }
        return null;
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<RequestDetails> loader, RequestDetails data) {
        mStatus.setText(data.getStatusText());
        mFullName.setText(data.getFullName());
        mDescription.setText(data.getDescription());
        mSolutionDescription.setText(data.getSolutionDescription());


        //get dummy SLARecoveryTime and ActualRecovery
        Random rand = new Random();

        double a = (rand.nextInt(72) + 1) * 3600 * 1000; //actual
        double b = a + (rand.nextInt(72) + 1) * 3600 * 1000; //SLA

        int progress = (int)((a / b) * 100);

        mTimeLine.setMax(100);
        mTimeLine.setProgress(progress);

        mCreatedAt.setText(DateUtils.formatDate(new Date(System.currentTimeMillis())));
        mActualRecovery.setText(DateUtils.formatDate(new Date((long)(System.currentTimeMillis() + a))));
        mSLARecovery.setText(DateUtils.formatDate(new Date((long)(System.currentTimeMillis() + b))));
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<RequestDetails> loader) {

    }
}
