package dsfn.edu.testapp.Utils;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by alexandrmakoed on 10/11/2016.
 */

public class DateUtils {

    private static String TAG = "DateUtils";

    private static SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy HH:mm");

    public static Date parseString(String s) {
        try {
            format.parse(s);
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }

        return null;
    }

    public static String formatDate(Date date) {
        return format.format(date);
    }
}
