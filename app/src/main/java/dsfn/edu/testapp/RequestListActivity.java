package dsfn.edu.testapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import dsfn.edu.testapp.Utils.DateUtils;
import dsfn.edu.testapp.models.RequestItem;
import dsfn.edu.testapp.tasks.RequestListLoadTask;
import dsfn.edu.testapp.tasks.RequestListLoadTaskCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class RequestListActivity extends AppCompatActivity implements RequestListLoadTaskCallback {

    private boolean mTwoPane;

    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        mRecyclerView = (RecyclerView) findViewById(R.id.request_list);
        assert mRecyclerView != null;
        setupRecyclerView((RecyclerView) mRecyclerView);

        if (findViewById(R.id.request_detail_container) != null) {
            mTwoPane = true;
        }

        new RequestListLoadTask(this).execute();
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        RequestItemRecyclerViewAdapter adapter = new RequestItemRecyclerViewAdapter(new ArrayList<RequestItem>());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void executionFinished(List<RequestItem> items) {
        if(items != null) mRecyclerView.setAdapter(new RequestItemRecyclerViewAdapter(items));
    }

    public class RequestItemRecyclerViewAdapter
            extends RecyclerView.Adapter<RequestItemRecyclerViewAdapter.RequestViewHolder> {

        private final List<RequestItem> mValues;

        public RequestItemRecyclerViewAdapter(List<RequestItem> items) {
            mValues = items;
        }

        @Override
        public RequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.request_list_content, parent, false);
            return new RequestViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final RequestViewHolder holder, int position) {
            holder.setItem(mValues.get(position));

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString(RequestDetailFragment.ARG_ITEM_ID, holder.mItem.getId());
                        RequestDetailFragment fragment = new RequestDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.request_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, RequestDetailActivity.class);
                        intent.putExtra(RequestDetailFragment.ARG_ITEM_ID, holder.mItem.getId());

                        context.startActivity(intent);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        class RequestViewHolder extends RecyclerView.ViewHolder {
            final View mView;
            final TextView mRequestNameView;
            final TextView mRequestNumberView;
            final TextView mRequestCreatedView;
            RequestItem mItem;

            RequestViewHolder(View view) {
                super(view);
                mView = view;
                mRequestNumberView = (TextView) view.findViewById(R.id.request_number);
                mRequestNameView = (TextView) view.findViewById(R.id.request_name);
                mRequestCreatedView = (TextView) view.findViewById(R.id.request_created);
            }

            void setItem(RequestItem item) {
                mItem = item;

                mRequestNameView.setText(item.getName());
                mRequestNumberView.setText(item.getRequestNumber().toString());
                mRequestCreatedView.setText(DateUtils.formatDate(item.getCreatedAt()));
            }
        }
    }
}
