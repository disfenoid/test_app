package dsfn.edu.testapp.models;

import java.util.Date;

/**
 * Created by alexandrmakoed on 10/11/2016.
 */

public class RequestItem {
    protected String id;
    protected String requestNumber;
    protected String name;
    protected Date createdAt;

    public RequestItem(String id, String requestNumber, String name, Date createdAt) {
        this.id = id;
        this.requestNumber = requestNumber;
        this.name = name;
        this.createdAt = createdAt;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRequestNumber() {
        return requestNumber;
    }

    public void setRequestNumber(String requestNumber) {
        this.requestNumber = requestNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
