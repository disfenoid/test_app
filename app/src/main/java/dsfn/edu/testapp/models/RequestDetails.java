package dsfn.edu.testapp.models;

import java.util.Date;

/**
 * Created by alexandrmakoed on 10/11/2016.
 */

public class RequestDetails {
    protected String id;
    protected String statusText;
    protected String fullName;
    protected String description;
    protected String solutionDescription;
    protected Date createdAt;
    protected Date actualRecover;
    protected Date slaRecoveryTime;

    public RequestDetails() {
    }

    public RequestDetails(String id, String statusText, String fullName, String description, String solutionDescription, Date createdAt, Date actualRecover, Date slaRecoveryTime) {
        this.id = id;
        this.statusText = statusText;
        this.fullName = fullName;
        this.description = description;
        this.solutionDescription = solutionDescription;
        this.createdAt = createdAt;
        this.actualRecover = actualRecover;
        this.slaRecoveryTime = slaRecoveryTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSolutionDescription() {
        return solutionDescription;
    }

    public void setSolutionDescription(String solutionDescription) {
        this.solutionDescription = solutionDescription;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getActualRecover() {
        return actualRecover;
    }

    public void setActualRecover(Date actualRecover) {
        this.actualRecover = actualRecover;
    }

    public Date getSlaRecoveryTime() {
        return slaRecoveryTime;
    }

    public void setSlaRecoveryTime(Date slaRecoveryTime) {
        this.slaRecoveryTime = slaRecoveryTime;
    }
}
