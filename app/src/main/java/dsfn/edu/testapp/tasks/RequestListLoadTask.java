package dsfn.edu.testapp.tasks;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import dsfn.edu.testapp.Utils.NetUtils;
import dsfn.edu.testapp.models.RequestItem;

/**
 * Created by alexandrmakoed on 10/11/2016.
 */

public class RequestListLoadTask extends AsyncTask<Void, Void, List<RequestItem>> {
    protected RequestListLoadTaskCallback callback;

    private String TAG = "RequestListLoadTask";

    public RequestListLoadTask(RequestListLoadTaskCallback callback) {
        this.callback = callback;
    }

    @Override
    protected List<RequestItem> doInBackground(Void... voids) {
        try {
            URI uri = new URI("http://mobile.atrinity.ru/api/service?ApiKey=e8e6a311d54985a067ece5a008da280a&Login=d_blinov&Password=Passw0rd&ObjectCode=300&Action=GET_LIST&Fields[FilterID]=3CD0E650-4B81-E511-A39A-1CC1DEAD694D");

            URL url = new URL(uri.toASCIIString());

            String jsonString = NetUtils.loadJsonStringFromUrl(url);

            JSONArray resultArr = new JSONArray(jsonString);

            List<RequestItem> items = new ArrayList<>(resultArr.length());

            for(int i = 0; i < resultArr.length(); i++) {
                JSONObject obj = resultArr.getJSONObject(i);

                String id = obj.getString("RequestID");
                String requestNumber = obj.getString("RequestNumber");
                String name = obj.getString("Name");
                Date createdAt = new SimpleDateFormat("dd.mm.yyyy HH:mm").parse(obj.getString("CreatedAt"));

                items.add(i, new RequestItem(id, requestNumber, name, createdAt));
            }

            return items;

        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, e.getMessage());
        }
        return null;
    }

    @Override
    protected void onPostExecute(List<RequestItem> requestItems) {
        super.onPostExecute(requestItems);
        callback.executionFinished(requestItems);
    }
}
