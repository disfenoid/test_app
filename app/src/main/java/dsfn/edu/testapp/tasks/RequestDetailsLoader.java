package dsfn.edu.testapp.tasks;

import android.content.Context;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.Loader;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import dsfn.edu.testapp.Utils.DateUtils;
import dsfn.edu.testapp.Utils.NetUtils;
import dsfn.edu.testapp.models.RequestDetails;

/**
 * Created by alexandrmakoed on 10/11/2016.
 */

public class RequestDetailsLoader extends Loader<RequestDetails> {

    private static String TAG = "RequestDetailLoader";

    private String mRequestItemID;

    public static String REQ_ITEM_ID = "RequestItemID";
    public static int RDLOADER_ID = 2;

    private Task task;

    public RequestDetailsLoader(Context context, Bundle args) {
        super(context);
        if(args != null){
            mRequestItemID = args.getString(REQ_ITEM_ID);
        }
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();
        task = new Task();
        task.execute();
    }

    @Override
    protected void onForceLoad() {
        super.onForceLoad();
        if(task != null) {
            task.cancel(true);
        }
        task = new Task();
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    class Task extends AsyncTask<Void, Void, RequestDetails> {
        @Override
        protected RequestDetails doInBackground(Void... params) {
            try {
                URI uri = new URI("http://mobile.atrinity.ru/api/service?ApiKey=e8e6a311d54985a067ece5a008da280a&Login=d_blinov&Password=Passw0rd&ObjectCode=300&Action=GET_INFO&Fields[RequestID]=" + mRequestItemID);
                String jsonString = NetUtils.loadJsonStringFromUrl(new URL(uri.toASCIIString()));

                if(jsonString != null) {
                    JSONObject obj = new JSONObject(jsonString).getJSONObject("Request");

                    String id = obj.getString("RequestID");
                    String statusText = obj.getString("StatusText");
                    String fullName = obj.getJSONObject("ContactContactID").getJSONObject("UserID").getString("FullName");
                    String description = obj.getString("Description");
                    String solutionDescription = obj.getString("SolutionDescription");
                    Date createdAt = DateUtils.parseString(obj.getString("CreatedAt"));
                    Date actualRecoveryTime = DateUtils.parseString(obj.getString("ActualRecoveryTime"));
                    Date slaRecoveryTime = DateUtils.parseString(obj.getString("SLARecoveryTime"));

                    return new RequestDetails(
                            id,
                            statusText,
                            fullName,
                            description,
                            solutionDescription,
                            createdAt,
                            actualRecoveryTime,
                            slaRecoveryTime
                    );
                }

            } catch (Exception e) {
                Log.d(TAG, e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(RequestDetails requestDetails) {
            super.onPostExecute(requestDetails);
            deliverResult(requestDetails);
        }
    }
}
