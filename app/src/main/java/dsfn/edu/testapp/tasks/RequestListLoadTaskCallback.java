package dsfn.edu.testapp.tasks;

import java.util.List;

import dsfn.edu.testapp.models.RequestItem;

/**
 * Created by alexandrmakoed on 10/11/2016.
 */

public interface RequestListLoadTaskCallback {
    void executionFinished(List<RequestItem> items);
}
